import React, { useEffect, useState } from 'react'
import { NewTweet } from '../components/NewTweet'
import '../styles/Main.css'
import { TweetList } from '../components/TweetList'
import Paginate from '../components/Paginate'
import jwt_decode from 'jwt-decode'

function sortMessages(messages) {
    // Parse the date strings into Date objects
    messages.forEach((message) => {
        message.timestamp_utc_iso8601.S = new Date(
            Date.parse(message.timestamp_utc_iso8601.S)
        )
    })

    // Sort the messages in ascending order by their date of creation
    const sortedMessages = messages.sort(
        (a, b) => a.timestamp_utc_iso8601.S - b.timestamp_utc_iso8601.S
    )

    // Reverse the array to sort the messages in descending order
    return sortedMessages.reverse()
}

export const Home = () => {
    const [content, setContent] = useState('')
    const [messages, setMessages] = useState([])
    const [user, setUser] = useState(null)
    const [loading, setLoading] = useState(true)
    const [render, setRender] = useState(false)

    const url = window.location.href
    const path = window.location.hash

    useEffect(() => {
        if (path === "") {
            window.location.replace('https://twitter-auth.auth.eu-west-1.amazoncognito.com/login?client_id=55dtei6d3l95ihr2tobjqdp8aj&response_type=token&scope=email+openid+profile&redirect_uri=https://d2nxqm2jsn06wy.cloudfront.net/index.html')
        } else {
            const regex = new RegExp(/(?<=\#id_token=)(.*?)(?=\&access_token=)/);
            setUser(jwt_decode(url.match(regex)[0]).nickname);
        }
    }, [url, path]);

    useEffect(() => {
        fetch('https://plr9xpnuz9.execute-api.eu-west-1.amazonaws.com/PROD/', {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
        })
            .then((res) => res.json())
            .then((data) => setMessages(sortMessages(data.body.Items)))
            .then(() => setLoading(false))
    }, [])

    const handleNewTweet = (event) => {
        event.preventDefault()

        const formData = {
            sender: user,
            content: content,
            timestamp_utc_iso8601: {
                S: Date.now(),
            },
            likes: {
                N: '0',
            },
            channel_id: {
                S: 'apollo',
            },
        }
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(formData),
        }

        fetch(
            'https://plr9xpnuz9.execute-api.eu-west-1.amazonaws.com/PROD/',
            requestOptions
        ).then((response) => response.json())
        setContent('')
        setRender(!render)
    }

    useEffect(() => {
        setLoading(true)
        fetch('https://plr9xpnuz9.execute-api.eu-west-1.amazonaws.com/PROD/', {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
        })
            .then((res) => res.json())
            .then((data) => setMessages(sortMessages(data.body.Items)))
            .then(() => setLoading(false))
    }, [render])

    return (
        <>
            <div className="home">
                <h1>Home</h1>
            </div>
            <NewTweet
                content={content}
                setContent={setContent}
                handleNewTweet={handleNewTweet}
            />
            {loading ? (
                <div className="lds-ring">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            ) : (
                <div className="tweets">
                    <TweetList messages={messages} />
                </div>
            )}
        </>
    )
}
