import React from 'react'
import { Trend } from '../trending/Trend'
import { SettingsIcon } from '../../images/svg/svgs'

export const TrendsList = () => {
    const trends = [
        {
            name: 'AWS',
            topic: 'Trending in France',
            tweets: '3,700k',
        },
        {
            name: 'React',
            topic: 'Trending in France',
            tweets: '37k',
        },
        {
            name: 'Lambda',
            topic: 'Trending in France',
            tweets: '3,267',
        },
        {
            name: 'Bucket S3',
            topic: 'Trending in France',
            tweets: '27k',
        },
        {
            name: 'Fullstack development',
            topic: 'Trending in France',
            tweets: '481k',
        },
    ]
    return (
        <div>
            <div className="trends-for-you flex-space-between">
                <h1 className="m-0">Trends for you</h1>
                <SettingsIcon />
            </div>
            <div className="trends">
                {trends.map((trend) => (
                    <Trend trend={trend} />
                ))}
            </div>
        </div>
    )
}
