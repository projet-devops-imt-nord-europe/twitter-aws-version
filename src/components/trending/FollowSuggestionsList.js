import React from 'react'
import '../../styles/FollowList.css'
import { SmallAvatar } from "../../images/avatars";

export const FollowSuggestionsList = () => {
    return (
        <>
            <div className="flex-space-between">
                <h1 className="m-0">Who To Follow</h1>
            </div>
            <div className="follow-cards">
            <div className="who-to-follow">
                <div className="left">
                    <SmallAvatar width="48" image="https://upload.wikimedia.org/wikipedia/fr/thumb/7/7e/Nexity-logo.svg/1200px-Nexity-logo.svg.png" />
                </div>
                <div className="middle">
                    <span className="follow-card-name">@eric_sigoillot</span>
                    <span className="follow-card-handle">Fabulous DevOps at Nexity</span>
                </div>
                <div className="right">
                    <div className="btn follow-btn text-center">Follow</div>
                </div>
            </div>
                <div className="who-to-follow">
                    <div className="left">
                        <SmallAvatar width="48" image="https://imt-nord-europe.fr/wp-content/uploads/2021/08/cropped-IMT_Nord_Europe_.png" />
                    </div>
                    <div className="middle">
                        <span className="follow-card-name">@mikael_desertot</span>
                        <span className="follow-card-handle">Living timetable and computer scientist</span>
                    </div>
                    <div className="right">
                        <div className="btn follow-btn text-center">Follow</div>
                    </div>
                </div>
            </div>
        </>
    )
}
