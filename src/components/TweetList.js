import React, { useEffect, useState } from 'react'
import TweetCard from './TweetCard'
import { AiFillPlusCircle } from 'react-icons/all'
import Paginate from './Paginate'
export const TweetList = ({ messages }) => {
    const [currentPage, setCurrentPage] = useState(1)
    const [postsPerPage] = useState(10)

    const indexOfLastPost = currentPage * postsPerPage
    const indexOfFirstPost = indexOfLastPost - postsPerPage
    const currentPosts = messages.slice(indexOfFirstPost, indexOfLastPost)

    const paginate = (pageNumber) => {
        setCurrentPage(pageNumber)
    }

    const previousPage = () => {
        if (currentPage !== 1) {
            setCurrentPage(currentPage - 1)
        }
    }

    const nextPage = () => {
        if (currentPage !== Math.ceil(messages.length / postsPerPage)) {
            setCurrentPage(currentPage + 1)
        }
    }

    return (
        <>
            {currentPosts.map((tweet, index) => (
                <TweetCard key={index} tweet={tweet} />
            ))}
            <Paginate
                postsPerPage={postsPerPage}
                totalPosts={messages.length}
                paginate={paginate}
                previousPage={previousPage}
                nextPage={nextPage}
                currentPage={currentPage}
            />
        </>
    )
}
