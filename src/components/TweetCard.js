import React, { Component } from 'react'
import '../styles/TweetCard.css'
import { SmallAvatar } from '../images/avatars'
import {
    TweetCommentIcon,
    TweetRetweetIcon,
    TweetLikeIcon,
    TweetSendIcon,
} from '../images/svg/svgs'

export default function TweetCard({ tweet, key }) {
    const profImageurl =
        'https://abs.twimg.com/sticky/default_profile_images/default_profile_400x400.png'

    return (
        <div className="tweet-card">
            <div className="left">
                <SmallAvatar width="48" image={profImageurl} />
            </div>
            <div className="right">
                <div className="tweet-card-head">
                    <span className="tweet-card-name">{tweet.sender.S}</span>

                    {/*<span className="tweet-card-handle" onClick={this.handleRouting}>{this.tweet.user.handle}</span>*/}

                    <span className="tweet-card-time">
                        {' '}
                        -{' '}
                        {tweet.timestamp_utc_iso8601.S.toLocaleTimeString(
                            'en-US'
                        )}
                    </span>
                </div>
                <div className="tweet-card-body">
                    <div className="tweet-card-content">
                        <p className="m-0">{tweet.content.S}</p>
                    </div>
                    <div className="tweet-card-footer">
                        <span className="flex-align-center">
                            <TweetCommentIcon />{' '}
                            <span className="tweet-cars-icon">0</span>
                        </span>
                        <span className="flex-align-center">
                            <TweetRetweetIcon />
                            <span className="tweet-cars-icon">0</span>
                        </span>
                        <span className="flex-align-center">
                            <TweetLikeIcon />
                            <span className="tweet-cars-icon">
                                {tweet.likes.N}
                            </span>
                        </span>
                        <span className="flex-align-center">
                            <TweetSendIcon />
                        </span>
                    </div>
                </div>
            </div>
        </div>
    )
}
