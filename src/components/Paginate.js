import React from 'react'
import PaginationWithDots from "../utils/PaginationWithDots";
const Paginate = ({
    postsPerPage,
    totalPosts,
    paginate,
    previousPage,
    nextPage,
    currentPage,
}) => {
    let pageNumbers = []

    for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
        pageNumbers.push(i)
    }

    if (pageNumbers.length > 5) {
        pageNumbers = PaginationWithDots(
            currentPage,
            pageNumbers[pageNumbers.length - 1]
        )
    }

    return (
        <div className="pagination-container">
            <ul className="pagination">
                <li className="page-number" id="chevron">
                    <a href="#" onClick={previousPage}>
                        {' '}
                        &#60;
                    </a>
                </li>
                {pageNumbers.map((number) => {
                    if (number === currentPage) {
                        return (
                            <li
                                key={number}
                                id="current-page"
                                className="page-number"
                            >
                                <a href="#" onClick={() => paginate(number)}>
                                    {number}
                                </a>
                            </li>
                        )
                    } else if (number === '...') {
                        return (
                            <li key={number} className="page-number">
                                {number}
                            </li>
                        )
                    } else {
                        return (
                            <li key={number} className="page-number">
                                <a href="#" onClick={() => paginate(number)}>
                                    {number}
                                </a>
                            </li>
                        )
                    }
                })}
                <li className="page-number" id="chevron">
                    <a href="#" onClick={nextPage}>
                        {' '}
                        &#62;
                    </a>
                </li>
            </ul>
        </div>
    )
}

export default Paginate
